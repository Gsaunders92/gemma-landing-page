<?php

// NOTE: Include any required files here
require_once "classes/VideoGeneratorUtils.class.php";
require_once "api/SessionHelper.php";

if (isset($_GET["dev"])) {
    // Load the local App class instead of the EOV App class
    require_once "classes/App.devclass.php";
}

if (isset($_REQUEST['clear'])) {
    setSessionCookie(null);

    $location = VideoGeneratorUtils::getProtocol() . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    $location = str_replace(array("?clear", "&clear"), "", $location);
    header("Location: " . $location);
}


// Build a query string for the embedded video
$playerQueryParams = array('_timeline' => false);

// If uid is set, create video authentication params
if (!empty($_REQUEST['uid'])) {
    require_once "classes/Authenticator.class.php";
    $authenticator = new Authenticator();

    $playerQueryParams['uid'] = $_REQUEST['uid'];
    $playerQueryParams['ts'] = time();
    $playerQueryParams['key'] = $authenticator->createAuthenticationString(
        $playerQueryParams['uid'],
        $playerQueryParams['ts']
    );
}

$queryString = "?" . http_build_query($playerQueryParams);
$iframeVideoLink = "/whitelabel-landing-page/project/videoPlayer.php{$queryString}";
//$iframeVideoLink = "https://preprod.rtcvid.net/ez_energy/videoPlayer.php{$queryString}"; // Working PURL: f19u5ap5mi132s

// If a base_url is not set in a config .ini file, we'll use this instead
$defaultBaseUrl = "http://whitelabel-landing.pb.com";
?>
<!DOCTYPE html>
<!--[if IE 7]><html class="fluidplayer ie ie7 lteie8 lteie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="fluidplayer ie ie8 lteie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="fluidplayer" lang="en">
<!--<![endif]-->

    <head>
        <?php require_once "partials/head-meta.php" ?>
    </head>

    <body>
        <div class="full-wrapper">
            <div class="custom-container container-box">
                <header role="banner">
                    <?php require_once "partials/header.php" ?>
                </header>

                <main role="main" class="clearfix">
                    <div class="col-xs-2 side-button-container">
                        <?php require_once "partials/sidebar-left.php" ?>
                    </div>
                    <div class="col-xs-8 player-column">
                        <?php require_once "partials/player.php" ?>
                    </div>
                    <div class="col-xs-2 promos-container">
                        <?php require_once "partials/sidebar-right.php" ?>
                    </div>
                </main>

                <p class="prepared-for-text">
                    <span id="jsPerson" data-dict-name="first_name">&nbsp</span>
                </p>

                <div class="timeline-wrapper">
                    <?php require_once "partials/timeline.php" ?>
                </div>
            </div>

            <footer role="contentinfo" class="custom-container">
                <?php require_once "partials/footer.php" ?>
            </footer>
        </div>

        <script src="dist/js/script.js"></script>

        <?php
        if (App::getSetting("google_analytics_enabled", false)) {
        ?>
          <!-- Remove or change this if you're using a different analytics provider -->
          <script>
            (function(i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', '<?php echo App::getSetting("google_analytics_id") ?>', 'auto');
            ga('send', 'pageview');
          </script>
          <?php
        }
        ?>

    </body>
</html>
